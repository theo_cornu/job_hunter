from elasticsearch import Elasticsearch
from flask import Flask

app = Flask(__name__)
from flask import request


@app.route("/get_job_offers", methods=["GET"])
def get_job_offers():
    keyword = request.args.get("keyword")
    city = request.args.get("city")
    print(keyword, city)
    res = get_offers_from_es(city, keyword)
    return res


def get_offers_from_es(city, keyword):
    elasticsearch: Elasticsearch = Elasticsearch(hosts=[{"host": "X.X.X.X", "port": 9200}])
    res = elasticsearch.search(
        index="table_name",
        body={"query": {"bool": {"must": {"match": {"skills": keyword}}, "filter": {"term": {"city": city}}}}},
    )
    return res
